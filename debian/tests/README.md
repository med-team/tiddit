Data for tiddit
=====================

	The test data for tiddit has been referenced from here: https://github.com/genome/somatic-snv-test-data which has further steps for creating the data
	All work is from public datasets and have been made with tools in the open domain.

