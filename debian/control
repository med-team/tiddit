Source: tiddit
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               cython3,
               python3-all-dev,
               python3-setuptools,
               python3-numpy,
               python3-pysam
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/tiddit
Vcs-Git: https://salsa.debian.org/med-team/tiddit.git
Homepage: https://github.com/SciLifeLab/TIDDIT
Rules-Requires-Root: no

Package: tiddit
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         python3-joblib
Description: structural variant calling
 TIDDIT is a tool to used to identify chromosomal rearrangements using
 Mate Pair or Paired End sequencing data. TIDDIT identifies intra and inter-
 chromosomal translocations, deletions, tandem-duplications and
 inversions, using supplementary alignments as well as discordant pairs.
 .
 TIDDIT has two analysis modules. The sv mode, which is used to search
 for structural variants. And the cov mode that analyse the read depth of
 a bam file and generates a coverage report.
